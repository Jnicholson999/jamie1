import java.util.Scanner;

public class Diamond {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter how thick you want the thickest part of your diamond to be.");
		int i = scan.nextInt();
		for (int o = 0; o < i; o++) {
			for (int q = o;q < i; q++){
				System.out.print(" ");
			}
			for (int j = 0; j < o+1; j++){
				//We want the amount of 'j's' that we have to print out
				System.out.print("* ");
			}		

			System.out.println();
		}
		for (int o = 1; o < i; o++) {
			for (int h = 0; h < o+1; h++){
				//We want the amount of 'j's' that we have to print out
				System.out.print(" ");
			}	
			for (int g = i;g > o; g--){
				System.out.print("* ");
			}	
			System.out.println();
		}
		// TODO Auto-generated method stub

	}

}