import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Arrays;

public class Hashmap {

	private LinkedList<String>[] keys;
	private LinkedList<Object>[] values;
	private int counter;

	public Hashmap() {
		keys = new LinkedList[8];
		values = new LinkedList[8];
		for(int i = 0; i < keys.length; i++) {
			keys[i] = new LinkedList<String>();
			values[i] = new LinkedList<Object>();
		}
	}

	private int hash(String data) {
		int temp = 0;
		int n = data.length();
		for(int i = 0; i < n; i++) {
			temp = temp + (int)data.charAt(i) * 31^(n-(i+1));
		}
		temp = temp + data.charAt(n-1);
		return Math.abs(temp%values.length);
	}


	public Object put(String key, Object value) {
		counter++;
		int index = hash(key);
		keys[index].add(key);
		values[index].add(value);
		if(counter > Math.sqrt(keys.length)) {
			resize();
		}
		return null;
	}

	private void resize() {
		counter = 0;
		LinkedList<String>[] tempKeys = Arrays.copyOf(keys, keys.length);
		LinkedList<Object>[] tempValues = Arrays.copyOf(values, values.length);
		keys = new LinkedList[(int) (Math.pow(tempKeys.length, 2))];
		values = new LinkedList[(int) (Math.pow(tempValues.length, 2))];
		for(int i = 0; i < keys.length; i++) {
			keys[i] = new LinkedList<String>();
			values[i] = new LinkedList<Object>();
		}
		for(int i = 0; i < tempKeys.length; i++) {
			for(int o = 0; o < tempKeys[i].size(); o++) {
				put(tempKeys[i].get(o), tempValues[i].get(o));
			}
		}
	}

	public void remove(String key) {
		int index = hash(key);
		int keyIndex = keys[index].indexOf(key);
		keys[index].remove(keyIndex);
		values[index].remove(keyIndex);
		counter--;
	}

	public Object get(String key) {
		int index = hash(key);
		int keyIndex = keys[index].indexOf(key);
		return values[index].get(keyIndex);
	}
}
