package ProjectEuler;

public class CollatzSequence {

	public static void main(String[] args) {
		
		int counter = 1;
		int longestCounter = 0;
		int longestSequence = 0;
		for(int i = 1; i < 1000000; i++) {
			int storeNumb = i;
			boolean a = true;
			while(a) {
				if(storeNumb == 1) {
					counter++;
					if(counter > longestCounter) {
						longestCounter = counter;
						longestSequence = i;
					}
					counter = 0;
					a = false;
				}
				else if(storeNumb%2 == 0) {
					storeNumb = storeNumb/2;
					counter++;
				}
				else {
					storeNumb = 3 * storeNumb + 1;
					counter++;
				}
			}
		}
		System.out.println(longestSequence);
		System.out.println(longestCounter);
		// TODO Auto-generated method stub

	}

}
