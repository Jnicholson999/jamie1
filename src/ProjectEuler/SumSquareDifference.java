package ProjectEuler;

public class SumSquareDifference {
	
	public static void main(String[] args) {
		
		int sumOfSquares = 0;
		int squareOfSum = 0;
		int difference = 0;
		
		for(int i = 1; i < 101; i++) {
			sumOfSquares = sumOfSquares + (i*i);
			squareOfSum = squareOfSum + i;
		}
		
		squareOfSum = squareOfSum * squareOfSum;
		difference = squareOfSum - sumOfSquares;
		System.out.println(difference);
		//25164150

	}

}
