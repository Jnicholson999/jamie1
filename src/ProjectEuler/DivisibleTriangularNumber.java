package ProjectEuler;

public class DivisibleTriangularNumber {

	public static void main(String[] args) {
		
		int TriNumb = 1;
		int TriVal = 1;
		int counter = 0;
		boolean a = true;
		
		outerloop:
		while(a) {
			for(int divisor = 1; divisor <= TriNumb/2; divisor++) {
				if(TriNumb%divisor == 0) {
					counter++;
				}
				if(counter >= 500) {
					a = false;
					break outerloop;
				}
			}
			counter = 0;
			TriVal++;
			TriNumb = TriNumb + TriVal;
		}
		
		System.out.println(TriNumb);
		//76576500
		// TODO Auto-generated method stub

	}

}
