import java.util.Scanner;

public class HollowDiamond {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter how many spaces thick you want the thickest part of your fancy hollow diamond to be.");
		int i = scan.nextInt();
		for (int o = 0; o < i; o++) {
			for (int q = o;q < i; q++){
				System.out.print(" ");
			}
			for (int j = 0; j < o+1; j++){
				if(j==0 || j==i-1 || j==o){
					System.out.print("* ");
				}
				else{
					System.out.print("  ");
				}
			}		

			System.out.println();
		}
		for (int o = 1; o < i; o++) {
			for (int h = 0; h < o+1; h++){
				//We want the amount of 'j's' that we have to print out
				System.out.print(" ");
			}	
			for (int g = i;g > o; g--){
				if(g==0 || g==o+1 || g==i){
					System.out.print("* ");
				}
				else{
					System.out.print("  ");
				}
			}	
			System.out.println();
		}
		// TODO Auto-generated method stub

	}

}