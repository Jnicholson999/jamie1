public class MergeSort {
	int r = 0;
	int l = 0;
	int m = 0;
	
	public static void main(String[] args) {
		
		int [] purpleq = {85, 59, 12, 45, 72, 100, 51, 75};
		// TODO Auto-generated method stub

		MergeSort ob = new MergeSort();
		ob.sort(purpleq,  0,  purpleq.length-1);
		for(int i = 0; i < 8; i++){
			System.out.println(purpleq[i]);
		}

	}
	
	void merge (int purpleq[], int l, int m, int r) {
		
		int n1 = m - l + 1;
		int n2 = r - m;
		int L[] = new int [n1];
		int R[] = new int [n2];
		for(int i = 0; i < n1; i++){
			L[i] = purpleq[l+i];
		}
		for(int i = 0; i < n2; i++){
			R[i] = purpleq[m+1+i];
		}
		int i = 0;
		int j = 0;
		int k = l;
		while(i < n1 && j < n2){
			if(L[i] <= R[j]){
				purpleq[k] = L[i];
				i++;
			}
			else {
				purpleq[k] = R[j];
				j++;
			}
			k++;
		}
		while(i < n1){
			purpleq[k] = L[i];
			k++;
			i++;
		}
		while(j < n2){
			purpleq[k] = R[j];
			k++;
			j++;
		}
		
	}
	
	void sort (int purpleq[], int l, int r) {
		
		if(l < r){
			int m = (l+r)/2;
			//sort the left
			sort(purpleq, l, m);
			//sort the right
			sort(purpleq, m+1, r);
			//merge the left and the right
			merge(purpleq, l, m, r);
		}
		
	}

}
