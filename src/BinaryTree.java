
public class BinaryTree <E extends Comparable<E>> {

	public E information;
	public BinaryTree link1;
	public BinaryTree link2;
	
	public BinaryTree(E element, BinaryTree a, BinaryTree b){
		information = element;
		link1 = a;
		link2 = b;
	}
	public BinaryTree(E element){
		information = element;
		link1 = null;
		link2 = null;
	}
	public void setInformation(E element){
		information = element;
	}
	public void setLink1(BinaryTree a){
		link1 = a;
	}
	public void setLink2(BinaryTree b){
		link2 = b;
	}
	public E getInformation(){
		return information;
	}
	public BinaryTree getLink1(){
		return link1;
	}
	public BinaryTree getLink2(){
		return link2;
	}

}
