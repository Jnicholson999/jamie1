public class BinarySearchTree <E extends Comparable<E>> {

	public BinaryTree head;
	public BinaryTree right1;
	public BinaryTree left1;

	public BinarySearchTree(){
		head = null;
	}


	public void add(E data){
		if(head == null){
			head = new BinaryTree(data);
		}
		else if(head.getInformation().compareTo(data) == 0 && head.getLink1() == null){
			head.setLink1(right1 = new BinaryTree(data));
		}
		else if(head.getInformation().compareTo(data) < 0 && head.getLink1() == null){
			head.setLink1(right1 = new BinaryTree(data));
		}
		else if(head.getInformation().compareTo(data) > 0 && head.getLink2() == null){
			head.setLink2(left1 = new BinaryTree(data));
		}
		else{
			boolean a = true;
			BinaryTree temp1 = head.link1;
			BinaryTree temp2 = head.link2;
			while(a){
				if(temp1 != null && temp1.getLink1() == null && temp1.getInformation().compareTo(data) <= 0){
					temp1.setLink1(right1 = new BinaryTree(data));
					a = false;
				}
				else if(temp1 != null && temp1.getLink2() == null && temp1.getInformation().compareTo(data) > 0){
					temp1.setLink2(left1 = new BinaryTree(data));
					a = false;
				}
				else if(temp2 != null && temp2.getLink1() == null && temp2.getInformation().compareTo(data) <= 0){
					temp2.setLink1(right1 = new BinaryTree(data));
					a = false;
				}
				else if(temp2 != null && temp2.getLink2() == null && temp2.getInformation().compareTo(data) > 0){
					temp2.setLink2(left1 = new BinaryTree(data));
					a = false;
				}
				else{
					if(temp1 != null && temp1.getInformation().compareTo(data) <= 0){
						temp1 = temp1.link1;
					}
					else if(temp1 != null && temp1.getInformation().compareTo(data) > 0){
						temp1 = temp1.link2;
					}
					else if(temp2.getInformation().compareTo(data) <= 0){
						temp2 = temp2.link1;
					}
					else if(temp2.getInformation().compareTo(data) > 0){
						temp2 = temp2.link2;
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void subtract(E data){
		@SuppressWarnings("rawtypes")
		BinaryTree temp;
		temp = head;
		if(temp.link1 == null && temp.link2 == null) {
			head = null;
		}
		boolean a = true;
		while(a){
			if(head.getInformation().compareTo(data) == 0) {
				if(head.link1.link1 == null && head.link1.link2 == null && head.link2.link1 == null && head.link2.link2 == null) {
					if(head.link1 == null && head.link2 != null) {
						head = head.link2;
						a = false;
					}
					else if(head.link1 != null && head.link2 == null) {
						head = head.link1;
						a = false;
					}
					else {
						head.link1.link2 = head.link2;
						head = head.link1;
						a = false;
					}
				}
				else {
					System.out.println("no can do");
					a = false;
				}
			}
			else if(temp.link1.getInformation().compareTo(data) == 0){
				if(temp.link1 != null && temp.link1.link1 == null && temp.link1.link2 == null){
					temp.link1 = null;
					a = false;
				}
				else if(temp.link1 != null && temp.link1.link1 == null){
					temp.link1 = temp.link1.link2;
					a = false;
				}
				else if(temp.link1 != null && temp.link1.link2 == null){
					temp.link1 = temp.link1.link1;
					a = false;
				}
				else{
					if(temp.link1 != null && temp.link1.link1 == null || temp.link1.link2 == null) {
						temp = temp.link1;
						a = false;
					}
					else if(temp.link2 != null && temp.link2.link1 == null || temp.link2.link2 == null) {
						temp = temp.link2;
						a = false;
					}
					else {
						System.out.println("That cannot be removed");
						a = false;
					}
				}
			}
			else if(temp.link2.getInformation().compareTo(data) == 0) {
				if(temp.link2 != null && temp.link2.link1 == null && temp.link2.link2 == null){
					temp.link2 = null;
					a = false;
				}
				else if(temp.link2 != null && temp.link2.link1 == null){
					temp.link2 = temp.link2.link2;
					a = false;
				}
				else if(temp.link2 != null && temp.link2.link2 == null){
					temp.link2 = temp.link2.link1;
					a = false;
				}
				else {
					if(temp.link2 != null && temp.link2.link1 == null || temp.link2.link2 == null) {
						temp = temp.link2;
						a = false;
					}
					else {
						System.out.println("That cannot be removed");
						a = false;
					}
				}
			}
			else if(temp.link1.getInformation().compareTo(data) > 0){
				temp.link1 = temp.link1.link2;
			}
			else if(temp.link2.getInformation().compareTo(data) > 0){
				temp.link2 = temp.link2.link2;
			}
			else if(temp.link1.getInformation().compareTo(data) < 0){
				temp.link1 = temp.link1.link1;
			}
			else if(temp.link2.getInformation().compareTo(data) < 0){
				temp.link2 = temp.link2.link1;
			}
			else{
				System.out.println("something's wrong");
			}
		}
	}
}
