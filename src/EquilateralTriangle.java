import java.util.Scanner;

public class EquilateralTriangle {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the length of the sides that you want your equilateral triangle to be.");
		int i = scan.nextInt();
		for (int o = 0; o < i; o++) {
			for (int q = o;q < i; q++){
				System.out.print(" ");
			}
			for (int j = 0; j < o+1; j++){
				//We want the amount of 'j's' that we have to print out
				System.out.print("* ");
			}
			System.out.println();
		}
		// TODO Auto-generated method stub

	}

}
