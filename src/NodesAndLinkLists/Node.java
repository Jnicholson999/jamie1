package NodesAndLinkLists;
public class Node {

	int value;
	Node link;
	public static void main(String[] args) {
		int x = 1;
		int y = 2;
		int z = 3;
		int w = 4;
		int v = 5;
		Node a = new Node(x);
		Node b = new Node(y);
		a.link = b;
		Node c = new Node(z);
		b.link = c;
		Node d = new Node(w);
		c.link = d;
		Node e = new Node(v);
		d.link = e;
		// TODO Auto-generated method stub

	}
	
	public Node(int x){
		this.value = x;
	}

}
