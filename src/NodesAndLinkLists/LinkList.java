package NodesAndLinkLists;

public class LinkList {

	private LinkListNode first;
	
	public static void main(String[] args) {
		LinkList list = new LinkList();
		list.add(2);
		list.add(81);
		list.add(3);
		list.add(5);
		System.out.println(list.remove(0));
		System.out.println(list.remove(1));
		System.out.println(list.get(0));
		System.out.println(list.get(3));
		
	}

	public void add(int value) {
		LinkListNode a = new LinkListNode();
		a.value = value;
		if(first == null){
			first = a;
		}
		else{
			LinkListNode temp = first;
			while(temp.next != null){
				temp = temp.next;
			}
			temp.next = a;
		}
	}
	public int get(int index){
		if(index < 0){
			return -1;
		}
		LinkListNode temp = first;
		for(int i = 0; i < index; i++){
			if(temp.next != null){
				temp = temp.next;
			}
			else{
				return -1;
			}
		}
		return temp.value;
	}
	public int remove(int index) {

		LinkListNode temp = first;
		//first case
		if(index == 0 && first!=null){
			int tempvalue = first.value;
			first = first.next;
			return tempvalue;
		}
		else if(first == null){
			return -1;
		}
		for(int i = 0; i < index-1; i++){
			if(temp.next == null){
				return -1;
			}
			temp = temp.next;
		}
		if(temp.next == null){
			return -1;
		}
		int tempvalue = temp.next.value;
		temp.next=temp.next.next;
		
		
		return tempvalue;
	}

}
