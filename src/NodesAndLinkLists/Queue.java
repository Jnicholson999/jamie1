package NodesAndLinkLists;

public class Queue {
	Node head;
	Node tail;
	Node temp;
	int size = 0;

	public void push(int value) {
		Node newer = new Node(value);
		if(size == 0){
			head = newer;
			head.link = tail;
		}
		else if(size == 1){
			tail = newer;
			head.link = tail;
		}
		else{
			temp = tail;
			tail.link = newer;
			tail = newer;
		}
		size++;
	}

	public int pop(){
		int value;
		value = tail.value;
		tail = temp;
		size--;
		return value;
	}

	public void peek() {
		System.out.println(head.value);
		System.out.println(tail.value);
	}

	public static void main(String[] args) {

		Queue smallStack = new Queue();
		smallStack.push(1);
		smallStack.push(2);
		smallStack.push(3);
		smallStack.push(4);
		smallStack.push(5);
		smallStack.pop();
		smallStack.peek();
		// TODO Auto-generated method stub

	}

}