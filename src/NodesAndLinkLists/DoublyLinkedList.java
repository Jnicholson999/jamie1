package NodesAndLinkLists;

public class DoublyLinkedList {

	private LinkListNode first;
	private LinkListNode last;
	
	public static void main(String[] args) {
		LinkList list = new LinkList();
		DoublyLinkedList list2 = new DoublyLinkedList();
		System.out.println("start");
		for(int i = 0; i < 100000; i++){
			list.add(i);
		}
		System.out.println("finito benito");
		for(int i = 0; i < 900000; i++){
			list2.add(i);
		}
		System.out.println("finito benito");
	}

	public void add(int value){
		LinkListNode a = new LinkListNode();
		if(first == null){
			first = a;
			first.value = value;
			last = first;
			return;
		}
		else{
			a.value = value;
			last.next = a;
			a.previous = last;
			last = a;
			return;
		}

	}

	public int get(int index){
		if(index < 0){
			return -1;
		}
		LinkListNode temp = new LinkListNode();
		for(int i = 0; i < index; i++){
			if(temp.next !=null){
				temp = temp.next;
			}
			else{
				return -1;
			}
		}
		return temp.value;
	}
	
	public int remove(int index){
		LinkListNode temp = new LinkListNode();
		if(index == 0 && first != null){
			return -1;
		}
		else if(first == null){
			return -1;
		}
		for(int i = 0; i < index-1; i++){
			if(temp.next == null){
				return -1;
			}
			temp = temp.next;
		}
		if(temp.next == null){
			return -1;
		}
		int tempvalue = temp.next.value;
		temp.next=temp.next.next;
		
		
		return tempvalue;
	}

}
