package NodesAndLinkLists;

public class LinkListMain {

	public static void main(String[] args) {
		Node a = new Node(8);
		Node b = new Node(2);
		a.link = b;
		Node c = new Node(3);
		b.link = c;
		Node d = new Node(7);
		c.link = d;
		
	}

}
