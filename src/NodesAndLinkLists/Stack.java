package NodesAndLinkLists;

public class Stack {
	
	Node tail;
	int size = 0;
	//int size = 5;
	class Node{
		int value;
		Node link;
		public Node(int value) {
			this.value = value;
		}
	}
	void push(int value) {
		Node newer = new Node(value);
		newer.link = tail;
		this.tail = newer;
		size++;
	}
	public int pop() {
		int value;
		value = tail.value;
		tail = tail.link;
		size--;
		return value;
	}
	public void peek() {
		System.out.println(tail.value);
	}
	
	public static void main(String[] args) {
		
		Stack smallStack = new Stack();
		smallStack.push(1);
		smallStack.push(2);
		smallStack.push(3);
		smallStack.push(4);
		smallStack.push(5);
		smallStack.pop();
		smallStack.peek();
		// TODO Auto-generated method stub
		
	}

}
