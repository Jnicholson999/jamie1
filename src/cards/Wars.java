package cards;

public class Wars {

	public static void main(String[] args) {
		
		for(int z = 0; z < 100; z++){
			Deck deck = new Deck();
			deck.shuffleDeck();
			//deck.printDeck();
			deck.splitDeck();
			deck.playGame();
		}
	}

}
