package cards;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Deck {
	int c = 3;
	private Card [] arr = new Card [52];
	ArrayList<Card> deck1 = new ArrayList<Card>();
	ArrayList<Card> deck2 = new ArrayList<Card>();

	public Deck(){

		for(int i = 1; i < 14; i++){
			Card dumbCard = new Card("spades", i);
			arr[i-1] = dumbCard;
		}
		for(int i = 1; i < 14; i++){
			Card dumbCard = new Card("hearts", i);
			arr[i+12] = dumbCard;
		}
		for(int i = 1; i < 14; i++){
			Card dumbCard = new Card("diamonds", i);
			arr[i+25] = dumbCard;
		}
		for(int i = 1; i < 14; i++){
			Card dumbCard = new Card("clubs", i);
			arr[i+38] = dumbCard;
		}
	}

	public void printDeck(){

		for(int i = 0; i < 52; i++){
			System.out.println(arr[i]);

		}

	}

	public void shuffleDeck(){
		Random rando = new Random();
		for(int i = 0; i < 52; i++) {
			int tuesday = rando.nextInt(52);
			Card monday = null;
			monday = arr[i];
			arr[i] = arr[tuesday];
			arr[tuesday] = monday;
		}

	}


	//splits the deck
	public void splitDeck(){
		//		ArrayList<Card> deck1 = new ArrayList<Card>();
		//		ArrayList<Card> deck2 = new ArrayList<Card>();

		//deck1
		for(int i = 0; i < 26; i++){
			Card monday = arr[i];
			deck1.add(monday);
		}
		//		for(int i = 0; i < 26; i++){
		//			System.out.println(deck1.get(i));
		//		}
		//		System.out.println();

		//deck2
		for(int i = 26; i < 52; i++){
			Card monday = arr[i];
			deck2.add(monday);
		}
		//		for(int i = 0; i < 26; i++){
		//			System.out.println(deck2.get(i));
		//		}

	}

	public void playGame(){
		//		Scanner scan = new Scanner(System.in);
		System.out.println("Okay. Let's do this. Ready? 1, 2, 3 WAR");
		int o = 0;
		int x = 0;
		int y = 0;
		while(o<1){
			int p = 0;
			p++;
			if(p == 4){
				Random rando = new Random();
				for(int i = 0; i < 52; i++) {
					int tuesday = rando.nextInt(52);
					Card monday = null;
					monday = arr[i];
					arr[i] = arr[tuesday];
					arr[tuesday] = monday;
				}
				for(int i = 0; i < deck2.size(); i++) {
					int tuesday = rando.nextInt(deck2.size());
					Card monday = null;
					monday = arr[i];
					arr[i] = arr[tuesday];
					arr[tuesday] = monday;
				}
				p = 0;
				System.out.println("shuffled?");
			}
			if(deck1.size() == 52){
				System.out.println("Player 1 wins");
				System.exit(0);
			}
			if(deck2.size() == 52){
				System.out.println("Player 2 wins");
				System.exit(0);
			}
			int i = compareCards();
			drawCards1();
			drawCards2();
			//System.out.println(deck1.size());
			//System.out.println(deck2.size());
			if(i == 0) {
				System.out.println("1 win"); 
				deck1.add(deck1.get(0));
				deck1.add(deck2.get(0));
				deck1.remove(0);
				deck2.remove(0);
				i = 3;
			}
			if(i == 1) {
				System.out.println("2 win");
				deck2.add(deck1.get(0));
				deck2.add(deck2.get(0));
				deck2.remove(0);
				deck1.remove(0);
				i = 3;
			}
			if(i == 2) {
				if(deck1.equals(49)){
					System.out.println("Player 1 wins");
					o++;
					System.exit(0);
				}
				if(deck2.equals(49)){
					System.out.println("Player 2 wins");
					o++;
					System.exit(0);
				}
				System.out.println("WAAARRRRRR");
				war(c);
				i = 3;
			}
			p++;
			System.out.println();
			Scanner scan = new Scanner(System.in);
			//scan.nextLine();			
			System.out.println(deck1);
			System.out.println(deck2);
			//			try {
			//				Thread.sleep(2000);
			//			} catch (InterruptedException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}
		}
		if(x > 0){
			System.out.println("Player 1 wins");
		}
		if(y > 0){
			System.out.println("Player 2 wins");
		}
	}

	public void drawCards1(){
		System.out.println(deck1.get(0));
	}

	public void drawCards2(){
		System.out.println(deck2.get(0));
	}

	public int compareCards(){
		if(deck1.get(0).getValue() > deck2.get(0).getValue()){
			return 0;
		}
		if(deck1.get(0).getValue() < deck2.get(0).getValue()){
			return 1;
		}
		else{
			return 2;
		}
	}

	public void war(int c){
//		System.out.println(deck1);
//		System.out.println(deck2);
		if(deck1.size() >= 49){
			System.out.println("Player 1 wins");
			System.exit(0);
		}
		if(deck2.size() >= 49){
			System.out.println("Player 2 wins");
			System.exit(0);
		}
		System.out.println("Card 1...");
		System.out.println(deck1.get(1));
		System.out.println(deck2.get(1));
		Scanner scan = new Scanner(System.in);
		System.out.println("Card 2...");
		System.out.println(deck1.get(2));
		System.out.println(deck2.get(2));
		System.out.println("LAST CARDDDDD");
		System.out.println(deck1.get(3));
		System.out.println(deck2.get(3));
		if(deck1.get(c).getValue() > deck2.get(c).getValue()){
			System.out.println("1 wins");
			deck1.add(deck2.get(0));
			deck1.add(deck1.get(0));
			deck1.remove(0);
			deck2.remove(0);
			deck1.add(deck2.get(0));
			deck1.add(deck1.get(0));
			deck1.remove(0);
			deck2.remove(0);
			deck1.add(deck2.get(0));
			deck1.add(deck1.get(0));
			deck1.remove(0);
			deck2.remove(0);
		}
		else if(deck1.get(c).getValue() < deck2.get(c).getValue()){
			System.out.println("2 wins");
			deck2.add(deck1.get(0));
			deck2.add(deck2.get(0));
			deck2.remove(0);
			deck1.remove(0);
			deck2.add(deck1.get(0));
			deck2.add(deck2.get(0));
			deck2.remove(0);
			deck1.remove(0);
			deck2.add(deck1.get(0));
			deck2.add(deck2.get(0));
			deck2.remove(0);
			deck1.remove(0);
		}
		else{
			c = c+4;
			war(c);
		}	

	}

}