package cards;

public class Card {
	
	//fields
	private String suit;
	private int value;
	
	//constructor
	public Card(String s, int v){
		value = v;
		suit = s;
	}
	
	//getters
	public int getValue(){
		return value;
	}
	public String getSuit(){
		return suit;
	}
	
	//setters
	public void setValue(int v){
		value = v;
	}
	public void setSuit(String s){
		suit = s;
	}
	
	//to String
	public String toString(){
		if(value == 1)
			return ("Ace of " + suit);
		
		if(value == 11)
			return ("Jack of " + suit);
		
		if(value == 12)
			return ("Queen of " + suit);
		
		if(value == 13)
			return ("King of " + suit);
		
		else
			return (value + " of " + suit);
		
	}
}
