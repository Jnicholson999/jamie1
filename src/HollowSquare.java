import java.util.Scanner;
public class HollowSquare {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the size you want your hollow (Fancy!) square to be.");
		int i = scan.nextInt();
		System.out.println(" ");
		for (int o = 0; o < i; o++){
			for (int j = 0; j < i; j++){
				if(o==0 || o==i-1 || j==0 || j==i-1) {
					System.out.print("* ");
				}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}	
		// TODO Auto-generated method stub

	}

}