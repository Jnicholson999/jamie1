package Recursion;
import java.util.Scanner;
public class FibbonaciSequence {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		// TODO Auto-generated method stub
		System.out.println("Which number in the Fibonacci Sequence do you want to be shown?");
		int x = scan.nextInt();
		System.out.println(fibonacci(x));
	}
	
	public static int fibonacci(int x) {
		if(x==0){
			return 0;
		}
		else if (x==1) {
			return 1;
		}
		else {
			return fibonacci(x-1) + fibonacci(x-2);
		}
	}

}