package Recursion;
import java.util.Scanner;

public class TowersOfHanoi {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("How many discs do you want on the Towers of Hanoi?");
		double x = scan.nextInt();
		System.out.println(towering(x));
		// TODO Auto-generated method stub

	}
	public static double towering(double x) {
		if(x == 0) {
			return 0;
		}
		if(x == 1){
			return 1;
		}
		else {
			return (Math.pow(2, x))-1;
		}
	}

}
