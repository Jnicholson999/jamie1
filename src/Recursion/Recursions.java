package Recursion;
import java.util.Scanner;

public class Recursions {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Pick a number");
		long x = scan.nextInt();
		System.out.println(factorial(x));
		// TODO Auto-generated method stub

	}
	public static long factorial(long x) {
		if(x == 1) {
			return 1;
		}
		else {
			return(x * factorial(x-1));
		}
		
	}

}
