import java.util.Scanner;

public class RightTriangle {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the length that you want the sides of your right triangle to be.");
		int i = scan.nextInt();
		for (int o = 0; o < i; o++){
			for (int j = 0; j < o+1; j++){
				//We want the amount of 'j's' that we have to print out
				System.out.print("* ");
			}
			System.out.println();
		}
		// TODO Auto-generated method stub

	}

}
