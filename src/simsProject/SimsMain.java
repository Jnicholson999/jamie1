package simsProject;
import java.util.Scanner;
public class SimsMain {
	public static void main(String[] args){
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		Sims numberone = new Sims ("Hi","Bye");
		//Aims number two = new Aims ("Hello", "Goodbye");
		//Aims number three = new Aims ("Hey", "See You");
		int a = 1;
		while(a==1){
			System.out.print("What would you like your Sim to do? ");
			System.out.println("They could shower, eat, sleep, TV, chat, or bathroom");
			String action = scan.next();
			if(action.equals ("shower")) {
				numberone.shower();
			}
			else if(action.equals ("eat")) {
				numberone.eat();
			}
			else if(action.equals ("sleep")) {
				numberone.sleep();
			}
			else if(action.equals ("bathroom")) {
				numberone.bladder();
			}
			else if(action.equals ("tv")) {
				numberone.tv();
			}
			else{
				System.out.println("Your Sim cannot do that, or you wrote the wrong command. Try again.");
			}
	}
	}
}
