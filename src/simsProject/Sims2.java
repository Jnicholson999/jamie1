package simsProject;
import java.util.Scanner;
public class Sims2 {
	Scanner scan = new Scanner(System.in);


//	public void state(){
//		int hunger = 0;
//		int hygiene = 100;
//		int bladder = 0;
//		int energy = 100;
//		int fun = 100;
//		int social = 100;
//	}
	public void character() {
		int cookingskill = 0;
		int fishingskill = 0;
		int altheticskill = 0;
		int mechanicskill = 0;
		int musicskill = 0;	
		int age = 0;
	}
	public void action() {
		int hunger = 0;
		int hygiene = 100;
		int bladder = 0;
		int energy = 100;
		int fun = 100;
		int social = 100;
		int friendship = 0;
		System.out.print("What would you like your Sim to do? ");
		System.out.println("They could shower, eat, sleep, watch TV, chat, or use the bathroom");
		String action = scan.next();
		if(action.equals("eat")) {
			System.out.println("Your Sim has grabbed some food.");
			System.out.println("Your hunger has decreased.");
			if(hunger > 25){
				hunger=hunger-25;
			}
			else{
				hunger = 0;
			}
		}
		if(action.equals("shower")){
			System.out.println("Your Sim has taken a shower");
			System.out.println("Their hygiene has increased");
			if(hygiene < 75){
				hygiene=hygiene+25;
			}
			else{
				hygiene = 100;
			}
			System.out.println(hygiene);
		}
		if(action.equals("bathroom")){
			System.out.println("Your Sim has used the bathroom.");
			System.out.println("Their bladder is empty.");
			bladder=0;
			System.out.println(bladder);
		}
		if(action.equals("sleep")){
			System.out.println("Your Sim has slept");
			System.out.println("Their energy has gone up");
			if(energy < 75){
				energy=energy+25;
			}
			else{
				energy = 100;
			}
			System.out.println(energy);
		}
		if(action.equals("tv")){
			System.out.println("Your Sim has watched some TV");
			System.out.println("Their fun has gone up");
			if(fun < 75){
				fun=fun+25;
			}
			else{
				fun=100;
			}
			System.out.println(fun);
		}
		if(action.equals("chat")){
			System.out.println("Your Sim has chatted with their friends");
			System.out.println("Both their social and their friendship with their friend has gone up");
			if(social < 75){
				social=social+25;
			}
			else{
				social=100;
			}
			if(friendship < 75){
				friendship=friendship+25;
			}
			else{
				friendship=100;
			}
		}
	}	

}
