package simsProject;
public class Sims {
	// state
	int hunger = 0;
	int hygiene = 100;
	int bladder = 0;
	int energy = 100;
	int fun = 100;
	int social = 100;
	String firstname = "n/a";
	String lastname = "n/a";
	int cookingskill = 0;
	int fishingskill = 0;
	int altheticskill = 0;
	int mechanicskill = 0;
	int musicskill = 0;
	int age = 0;
	String job = "n/a";
	int jobadvancement = 0;
	public Sims(String fn, String ln) {
		this.firstname = fn;
		this.lastname = ln;
	}
	public int getAge () {
		return this.age;
	}
	public void setAge (int nV) {
		if(nV > 100) {
			return;
		}
		else {
			this.age = nV;
		}
	}
	
	// behavior
	public void eat () {
		System.out.println("Your Sim has grabbed some food.");
		System.out.println("Your hunger has decreased.");
		if(hunger > 25){
			this.hunger=this.hunger-25;
		}
		else{
			this.hunger = 0;
		}
		System.out.println(hunger);
	}
	public void shower () {
		System.out.println("Your Sim has taken a shower");
		System.out.println("Their hygiene has increased");
		if(hygiene < 75){
			this.hygiene=this.hygiene+25;
		}
		else{
			this.hygiene = 100;
		}
		System.out.println(hygiene);

	}	
	public void bladder () {
		System.out.println("Your Sim has used the bathroom.");
		System.out.println("Their bladder is empty.");
		this.bladder=0;
		System.out.println(bladder);
	}
	public void sleep () {
		System.out.println("Your Sim has slept");
		System.out.println("Their energy has gone up");
		if(energy < 75){
			this.energy=this.energy+25;
		}
		else{
			this.energy = 100;
		}
		System.out.println(energy);
	}
	public void tv () {
		System.out.println("Your Sim has watched some TV");
		System.out.println("Their fun has gone up");
		if(fun < 75){
			this.fun=this.fun+25;
		}
		else{
			this.fun=100;
		}
		System.out.println(fun);
	}
	public static void main(String[] args) {
		Sims kewlness = new Sims("n/a", "n/a");
		System.out.println (kewlness.bladder);	
		kewlness.bladder();
		kewlness.eat();

		// TODO Auto-generated method stub

	}

}
