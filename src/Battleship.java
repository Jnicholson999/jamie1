import java.util.Scanner;

public class Battleship {
	static int rows;
	static int column;
	static String [][] ships1 = new String [10][10];
	static String [][] ships2 = new String [10][10];
	static int s = 5;
	static int s2 = 5;
	static int columnguess;
	static int columnguess2;
	static boolean win = false;
	static boolean one = false;
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String [][] pirate = new String [10][10];	
		String [][] pirate2 = new String [10][10];
		System.out.println("Welcome to Battleship! Player One, choose where you want your carrier to go");
		playerboard(ships1);
		playerboard(ships2);
		int t = 0;
		while(t < 5){
			if(s == 4){
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println("Player 1's Board");
				board(pirate);
				System.out.println("  1 2 3 4 5 6 7 8 9 10");
				String [] arr = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
				for(int o = 0; o < 10; o++) {
					System.out.print(arr[o]);
					for(int p = 0; p < 10; p++) {
						System.out.print(ships1 [o][p]);
					}
					System.out.println();
				}
				boolean retry = true;
				String vh = "";
				while(retry){
					System.out.println("Player 1, which row would you like to put your ship in?");
					String letter = scan.next();
					rows = row(letter);

					System.out.println("Player 1, which column would you like to put your ship in?");
					column = scan.nextInt()-1;

					System.out.println("Player 1, would you like to put your ship vertically or horizontally.");
					System.out.println("Enter v for vertically or h for horizontally.");
					vh = scan.next();
					if(!vh.equals("h") && !vh.equals("v")) {
						retry = true;
						System.out.println("You did not enter v or h. Enter your coordinates again");
					}
					else {
						boolean run = false;
						if (vh.equals ("v")){
							for(int k = rows; k < rows + s; k++){
								if (ships1 [k][column].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry = true;
									break;
								}
								else {
									retry = false;
								}
								if (run) {
									break;
								}
							}
						}
						if (vh.equals ("h")){
							for(int k = column; k < column + s; k++){
								if (ships1 [rows][k].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry = true;
									break;
								}
								else {
									retry = false;
								}
								if (run) {
									break;
								}
							}
						}
					}
				}
				if(vh.equals("h")) {
					placinghorizontal4p1();
					for(int o = 0; o < 10; o++) {
						for(int p = 0; p < 10; p++) {
							System.out.print(ships1 [o][p]);
						}
						System.out.println();
					}
				}
				else if(vh.equals("v")){
					placingvertical4p1();
					for(int o = 0; o < 10; o++) {
						for(int p = 0; p < 10; p++) {
							System.out.print(ships1 [o][p]);
						}
						System.out.println();
					}
				}
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println("Player 2's Board");
				board2(pirate2);
				System.out.println("  1 2 3 4 5 6 7 8 9 10");
				String [] arr2 = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
				for(int o = 0; o < 10; o++) {
					System.out.print(arr2[o]);
					for(int p = 0; p < 10; p++) {
						System.out.print(ships2 [o][p]);
					}
					System.out.println();
				}
				boolean retry2 = true;
				String vh1 = "";
				while(retry2){
					System.out.println("Player 2, which letter row would you like to put your ship in?");
					String letter2 = scan.next();
					rows = row(letter2);

					System.out.println("Player 2, which column would you like to put your ship in?");
					column = scan.nextInt()-1;

					System.out.println("Player 2, would you like to put your ship vertically or horizontally.");
					System.out.println("Enter v for vertically or h for horizontally.");
					vh1 = scan.next();
					if(!vh1.equals("h") && !vh1.equals("v")) {
						retry2 = true;
						System.out.println("You did not enter v or h. Enter your coordinates again");
					}
					else {
						boolean run = false;
						if (vh1.equals ("v")){
							for(int k = rows; k < rows + s; k++){
								if (ships2 [k][column].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry2 = true;
									break;
								}
								else {
									retry2 = false;
								}
								if (run) {
									break;
								}
							}
						}
						if (vh1.equals ("h")){
							for(int k = column; k < column + s; k++){
								if (ships2 [rows][k].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry2 = true;
									break;
								}
								else {
									retry2 = false;
								}
								if (run) {
									break;
								}
							}
						}
					}
					if(vh1.equals("h")) {
						placinghorizontal4p2();
						for(int o = 0; o < 10; o++) {
							for(int p = 0; p < 10; p++) {
								System.out.print(ships2 [o][p]);
							}
							System.out.println();
						}
					}
					else if(vh1.equals("v")){
						placingvertical4p2();
						for(int o = 0; o < 10; o++) {
							for(int p = 0; p < 10; p++) {
								System.out.print(ships2 [o][p]);
							}
							System.out.println();
						}
					}
				}
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
			}
			if(s == 1){
				break;
			}
			else{
				System.out.println("Player 1's Board");
				boardsetup(pirate);
				System.out.println("  1 2 3 4 5 6 7 8 9 10");
				String [] arr = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
				for(int o = 0; o < 10; o++) {
					System.out.print(arr[o]);
					for(int p = 0; p < 10; p++) {
						System.out.print(ships1 [o][p]);
					}
					System.out.println();
				}
				boolean retry = true;
				String vh = "";
				while(retry){
					System.out.println("Player 1, which row would you like to put your ship in?");
					String letter = scan.next();
					rows = row(letter);

					System.out.println("Player 1, which column would you like to put your ship in?");
					column = scan.nextInt()-1;

					System.out.println("Player 1, would you like to put your ship vertically or horizontally.");
					System.out.println("Enter v for vertically or h for horizontally.");
					vh = scan.next();
					if(!vh.equals("h") && !vh.equals("v")) {
						retry = true;
						System.out.println("You did not enter v or h. Enter your coordinates again");
					}
					else {
						boolean run = false;
						if (vh.equals ("v")){
							for(int k = rows; k < rows + s; k++){
								if (ships1 [k][column].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry = true;
									break;
								}
								else {
									retry = false;
								}
								if (run) {
									break;
								}
							}
						}
						if (vh.equals ("h")){
							for(int k = column; k < column + s; k++){
								if (ships1 [rows][k].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry = true;
									break;
								}
								else {
									retry = false;
								}
								if (run) {
									break;
								}
							}
						}
					}
					if(vh.equals("h")) {
						placinghorizontal1();
						for(int o = 0; o < 10; o++) {
							for(int p = 0; p < 10; p++) {
								System.out.print(ships1 [o][p]);
							}
							System.out.println();
						}
					}
					else if(vh.equals("v")){
						placingvertical1();
						for(int o = 0; o < 10; o++) {
							for(int p = 0; p < 10; p++) {
								System.out.print(ships1 [o][p]);
							}
							System.out.println();
						}
					}
				}

				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println("Player 2's Board");
				boardsetup2(pirate2);
				System.out.println("  1 2 3 4 5 6 7 8 9 10");
				String [] arr2 = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
				for(int o = 0; o < 10; o++) {
					System.out.print(arr2[o]);
					for(int p = 0; p < 10; p++) {
						System.out.print(ships2 [o][p]);
					}
					System.out.println();
				}
				boolean retry2 = true;
				String vh1 = "";
				while(retry2){
					System.out.println("Player 2, which letter row would you like to put your ship in?");
					String letter2 = scan.next();
					rows = row(letter2);

					System.out.println("Player 2, which column would you like to put your ship in?");
					column = scan.nextInt()-1;

					System.out.println("Player 2, would you like to put your ship vertically or horizontally.");
					System.out.println("Enter v for vertically or h for horizontally.");
					vh1 = scan.next();
					if(!vh1.equals("h") && !vh1.equals("v")) {
						retry2 = true;
						System.out.println("You did not enter v or h. Enter your coordinates again");
					}
					else {
						boolean run = false;
						if (vh1.equals ("v")){
							for(int k = rows; k < rows + s; k++){
								if (ships2 [k][column].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry2 = true;
									break;
								}
								else {
									retry2 = false;
								}
								if (run) {
									break;
								}
							}
						}
						if (vh1.equals ("h")){
							for(int k = column; k < column + s; k++){
								if (ships2 [rows][k].equals("1 ")){
									System.out.println("You are not allowed to go there. Try again.");
									run = true;
									retry2 = true;
									break;
								}
								else {
									retry2 = false;
								}
								if (run) {
									break;
								}
							}
						}
					}
				}

				if(vh1.equals("h")) {
					placinghorizontal2();
					for(int o = 0; o < 10; o++) {
						for(int p = 0; p < 10; p++) {
							System.out.print(ships2 [o][p]);
						}
						System.out.println();
					}
				}
				else if(vh1.equals("v")){
					placingvertical2();
					for(int o = 0; o < 10; o++) {
						for(int p = 0; p < 10; p++) {
							System.out.print(ships2 [o][p]);
						}
						System.out.println();
					}
				}

				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
				System.out.println();
			}
			t++;
		}
		//everything above this is the setup for the game
		//below this is actual game play
		int b = 0;
		while(b < 1){
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("Player 1, choose the row you think Player 2's ship is.");
			String guess = scan.next();
			rows = row(guess);

			System.out.println("Player 1, choose the column you think Player 2's ship is in.");
			columnguess = scan.nextInt()-1;

			if(ships2 [rows][columnguess].equals ("1 ")) {
				System.out.println("Hit!");
				pirate [rows][columnguess] = ("X ");
				ships2[rows][columnguess] = ("0 ");
				board(pirate);
			}
			else {
				System.out.println("Miss");
				pirate [rows][columnguess] = ("O ");
				board(pirate);
			}
			for(int o = 0; o < 10; o++) {
				for(int p = 0; p < 10; p++) {
					if (ships2[o][p].equals ("1 ")){
						one = true;
						break;
					}

				}	
			}
			if (one) {
				win = false;
				one = false;
			}
			else {
				win = true;
			}
			if (win) {
				System.out.println("Player 1 wins!");
				System.exit(0);
			}
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("Player 2, choose the row you think Player 1's ship is.");
			String guess2 = scan.next();
			rows = row(guess2);

			System.out.println("Player 2, choose the column you think Player 1's ship is in.");
			columnguess2 = scan.nextInt()-1;

			if(ships1 [rows][columnguess2].equals ("1 ")) {
				System.out.println("Hit!");
				pirate2 [rows][columnguess2] = ("X ");
				ships1[rows][columnguess2] = ("0 ");
				board2(pirate2);
			}
			else {
				System.out.println("Miss");
				pirate2 [rows][columnguess2] = ("O ");
				board2(pirate2);

			}
			for(int o = 0; o < 10; o++) {
				for(int p = 0; p < 10; p++) {
					if (ships1[o][p].equals ("1 ")){
						win = false;
						one = true;
						break;
					}
				}	
			}
			if (one) {
				win = false;
				one = false;
			}
			else {
				win = true;
			}
			if (win) {
				System.out.println("Player 1 wins!");
				System.exit(0);
			}
		}
		// TODO Auto-generated method stub
	}

	public static void boardsetup(String [][] pirate) {
		//this prints the board each time
		String [] arr = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
		System.out.println("  1 2 3 4 5 6 7 8 9 10");
		for(int o = 0; o < 10; o++) {
			System.out.print(arr[o]);
			for(int p = 0; p < 10; p++) {
				pirate [o][p] = ("* ");
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	public static void boardsetup2(String [][] pirate2) {
		//this prints the board each time
		String [] arr = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
		System.out.println("  1 2 3 4 5 6 7 8 9 10");
		for(int o = 0; o < 10; o++) {
			System.out.print(arr[o]);
			for(int p = 0; p < 10; p++) {
				pirate2 [o][p] = ("* ");
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	public static void board(String [][] pirate) {
		//this prints the board each time
		String [] arr = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
		System.out.println("  1 2 3 4 5 6 7 8 9 10");
		for(int o = 0; o < 10; o++) {
			System.out.print(arr[o]);
			for(int p = 0; p < 10; p++) {
				System.out.print (pirate [o][p]);
			}
			System.out.println();
		}
	}
	public static void board2(String [][] pirate2) {
		//this prints the board each time
		String [] arr = {"A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J "};
		System.out.println("  1 2 3 4 5 6 7 8 9 10");
		for(int o = 0; o < 10; o++) {
			System.out.print(arr[o]);
			for(int p = 0; p < 10; p++) {
				System.out.print (pirate2 [o][p]);
			}
			System.out.println();
		}
	}
	public static void playerboard(String [][] ships) {
		for(int o = 0; o < 10; o++) {
			for(int p = 0; p < 10; p++) {
				ships [o][p] = ("0 ");
			}
		}
	}
	public static int row(String r) {
		//this checks where to put the ships or tries into a place with a letter

		if(r.equalsIgnoreCase("a")) {
			return 0;
		}
		if(r.equalsIgnoreCase("b")) {
			return 1;
		}
		if(r.equalsIgnoreCase("c")) {
			return 2;
		}
		if(r.equalsIgnoreCase("d")){
			return 3;
		}
		if(r.equalsIgnoreCase("e")) {
			return 4;
		}
		if(r.equalsIgnoreCase("f")) {
			return 5;
		}
		if(r.equalsIgnoreCase("g")) {
			return 6;
		}
		if(r.equalsIgnoreCase("h")) {
			return 7;
		}
		if(r.equalsIgnoreCase("i")) {
			return 8;
		}
		if(r.equalsIgnoreCase("j")) {
			return 9;
		}
		return 0;
	}
	public static void placinghorizontal1(){
		//this puts a ship horizontally in the place of the 0 in the ships array
		for(int k = column; k < column+s; k++) {
			ships1[rows][k] = ("1 ");
		}
		s--;
	}
	public static void placingvertical1(){
		//this one puts a ship vertically in the place of the 0 in the array
		for(int k = rows; k < rows+s; k++) {
			ships1[k][column] = ("1 ");
		}
		s--;
	}
	public static void placinghorizontal2(){
		//this puts a ship horizontally in the place of the 0 in the ships array
		for(int k = column; k < column+s2; k++) {
			ships2[rows][k] = ("1 ");
		}
		s2--;
	}
	public static void placingvertical2(){
		//this one puts a ship vertically in the place of the 0 in the array
		for(int k = rows; k < rows+s2; k++) {
			ships2[k][column] = ("1 ");
		}
		s2--;

	}
	public static void placingvertical4p2(){
		//this one puts a ship vertically in the place of the 0 in the array
		for(int k = rows; k < rows+s2; k++) {
			ships2[k][column] = ("1 ");
		}
	}	
	public static void placinghorizontal4p2(){
		//this puts a ship horizontally in the place of the 0 in the ships array
		for(int k = column; k < column+s2; k++) {
			ships2[rows][k] = ("1 ");
		}
	}
	public static void placinghorizontal4p1(){
		//this puts a ship horizontally in the place of the 0 in the ships array
		for(int k = column; k < column+s; k++) {
			ships1[rows][k] = ("1 ");
		}	
	}
	public static void placingvertical4p1(){
		//this one puts a ship vertically in the place of the 0 in the array
		for(int k = rows; k < rows+s; k++) {
			ships1[k][column] = ("1 ");
		}
		
	}

}