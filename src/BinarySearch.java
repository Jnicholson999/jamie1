
public class BinarySearch {

	public static void main(String[] args) {

		int [] purpleq = {1, 2, 5, 6, 7, 8, 43, 87, 425, 758, 759};
		System.out.println(binarySearch(0, 10, 759, purpleq));

	}

	public static int binarySearch(int lowindex, int highindex, int target, int []purpleq){
		int midindex = lowindex+(highindex-lowindex)/2;
		
		if(purpleq[midindex] > target){
			return binarySearch(0, midindex, target, purpleq);
		}
		
		else if(purpleq[midindex] < target){
			return binarySearch(midindex+1, highindex, target, purpleq);
		}
		
		else if(purpleq[midindex] == target){
			return midindex;
		}
		
		return -1;
	}
}