import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayList implements List {
	Object[] arr;
	
	public ArrayList() {
		arr = new Object[0];
	}
	@Override
	public boolean add(Object arg0) {
		Object[] temp = new Object[arr.length+1];
		for(int i = 0; i < arr.length; i++) {
			temp[i] = arr[i];
		}
		temp[arr.length] = arg0;
		arr = temp;
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void add(int arg0, Object arg1) {
		Object[] temp = new Object[arr.length+1];
		for(int i = 0; i < arr.length; i++) {
			if(i < arg0) {
				temp[i] = arr[i];
			}
			else if (i == arg0) {
				temp[i] = arg1;
			}
			else {
				temp[i] = arr[i];
			}
		}
		arr = temp;
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		arr = new Object[0];
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object o) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == o){
				return true;
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		return arr[index];
	}

	@Override
	public int indexOf(Object o) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == o) {
				return i;
			}
		}
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		if(arr.length == 0) {
			return true;
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object o) {
		for(int i = arr.length; i > 0; i--) {
			if(arr[i] == o) {
				return i;
			}
		}
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == o) {
				remove(i);
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object remove(int index) {
		Object[] temp = new Object[arr.length-1];
		for(int i = 0; i < arr.length; i++) {
			if(i != index) {
				temp[i] = arr[i];
			}
		}
		arr = temp;
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object set(int index, Object element) {
		arr[index] = element;
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return arr.length;
	}

	@Override
	public List subList(int fromIndex, int toIndex) {
		ArrayList temp = new ArrayList();
		for(int i = fromIndex; i < toIndex; i++) {
			temp.add(i);
		}
		// TODO Auto-generated method stub
		return temp;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return arr;
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
